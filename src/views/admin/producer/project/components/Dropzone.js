// Chakra imports
import { Button, Flex, Box, Input, useColorModeValue } from "@chakra-ui/react";
// Assets
import React, { useMemo, useState, useEffect } from "react";
import { useDropzone } from "react-dropzone";

//perview style

const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
  backgroundColor: "lightgrey",
  width: 100,
  height: 100,
};

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: "border-box",
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};

//drop style

const baseStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 16,
  borderColor: "#eeeeee",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const focusedStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

function Dropzone(props) {
  // const [files, setFiles] = useState([]);
  const { content, ...rest } = props;
  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    isFocused,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    maxFiles: 1,
    // accept: {
    // "image/*": [],
    // "image/jpeg": [],
    // "image/jpg": [],
    // "image/png": [],
    // "image/svg": [],
    // },
    // onDrop: (acceptedFiles) => {
    //   setFiles(
    //     Object.assign(acceptedFiles[0], {
    //       preview: URL.createObjectURL(acceptedFiles[0]),
    //     })
    //   );
    // },
  });

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isFocused ? focusedStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isFocused, isDragAccept, isDragReject]
  );
  // const bg = useColorModeValue("gray.100", "navy.700");
  // const borderColor = useColorModeValue("secondaryGray.100", "whiteAlpha.100");
  // useDropzone({
  //   accept: {
  //     "image/png": [".png", ".svg", ".jpeg"],
  //   },
  // });
  const files = acceptedFiles.map(
    (file) => props.action(file)
    // <li key={file.path}>
    //   {file.path} - {file.size} bytes
    // </li>
  );

  // const thumbs = files.map((file) => (
  //   <div style={thumb} key={file.name}>
  //     <div style={thumbInner}>
  //       <img
  //         src={file.preview}
  //         style={img}
  //         // Revoke data uri after image is loaded
  //         onLoad={() => {
  //           URL.revokeObjectURL(file.preview);
  //         }}
  //       />
  //     </div>
  //   </div>
  // ));

  // useEffect(() => {
  //   // Make sure to revoke the data uris to avoid memory leaks, will run on unmount
  //   return () => files.forEach((file) => URL.revokeObjectURL(file.preview));
  // }, []);

  // useEffect(() => {
  //   files.map((file) => console.log("FILE: ", file.name));
  // }, [files]);

  return (
    <>
      <Box
        align="center"
        justify="center"
        w="100%"
        h="max-content"
        minH="128px"
        mb={"10px"}
        cursor="pointer"
        {...getRootProps({ className: "dropzone", style })}
        {...rest}
      >
        <Input variant="main" {...getInputProps()} />
        {/* <p>Drag 'n' drop some files here, or click to select files</p> */}
        <em>{props.instructions}</em>
        {files && files.length > 0 && <p>File Accepted</p>}
      </Box>
      {/* <aside style={thumbsContainer}>{thumbs}</aside> */}
    </>
  );
}

export default Dropzone;
