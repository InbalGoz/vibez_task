import { useState } from "react";
import { useAuthContext } from "./useAuthContext";

export const useResetPassword = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const { dispatch } = useAuthContext();

  const recoverPassword = async (token) => {
    setIsLoading(true);
    setError(null);
    const response = await fetch(
      process.env.REACT_APP_API_URL +
        "/users/recover_password?reset_password_token=" +
        token,
      {
        method: "GET",
        headers: { "Content-Type": "application/json" },
      }
    );
    const json = await response.json();

    if (!response.ok) {
      setIsLoading(false);
      setError(json.error.message);
      return;
    }
    if (response.ok) {
      console.log("RESET RES: ", json);
      // save the user to local storage
      localStorage.setItem("token", JSON.stringify(json.token));
      localStorage.setItem("user_data", JSON.stringify(json.user));
      // update the auth context
      dispatch({ type: "LOGIN", payload: json.token });
      // update loading state
      setIsLoading(false);
      return json;
    }
  };

  return { recoverPassword, isLoading, error };
};

export const useRequestRecoverPassword = () => {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(null);

  const requestRecoverPassword = async (email) => {
    setIsLoading(true);
    setError(null);
    const response = await fetch(
      process.env.REACT_APP_API_URL + "/users/recover_password",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          email: email,
        }),
      }
    );
    const json = await response.json();

    if (!response.ok) {
      setIsLoading(false);
      setError(json.error.message);
      return false;
    }
    if (response.ok) {
      console.log("REQUEST RES: ", json);
      setIsLoading(false);
      return true;
    }
  };

  return { requestRecoverPassword, isLoading, error };
};
